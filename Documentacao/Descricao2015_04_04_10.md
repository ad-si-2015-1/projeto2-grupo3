 JO-KEN-PO

-- D E S C R I Ç Ã O --

  O Jo-Ken-Po cards é um jogo de duas pessoas. Os jogadores montam um card com cinco atributos e terão 100 pontos para serem distribuídos de acordo com a estratégia destes. 
Quando finalizarem o card o servidor irá comparar atributo por atributo, e ganha a primeira rodada quem tiver com mais atributos maiores que o outro, como podemos ver abaixo. 

-------------------------------------                 ------------------------------------
|           JOGADOR 1               |                 |        JOGADOR 2                 |    
|Atributo1: 25                      |                 |Atributo1: 40                     |
|Atributo2: 25                      |                 |Atributo2: 15                     |
|Atributo3: 25                      |                 |Atributo3: 5                      |
|Atributo4: 25                      |                 |Atributo4: 30                     |
|Atributo5: 0                       |                 |Atributo5: 10                     |
|                                   |                 |                                  |
-------------------------------------                 ------------------------------------ 
                       >>> O JOGADOR 2 GANHOU A PRIMEIRA RODADA! <<<

  Quando iniciar a segunda rodada, os jogadores vão novamente compor os pontos dos atributos do segundo card e comparar. Caso o JOGADOR 2 ganhe novamente, não haverá a terceira rodada. 
Se o JOGADOR 1 ganhar haverá a terceira rodada. Caso empate a terceira rodada, haverá uma rodada em que o primeiro jogador que tiver o atributo maior ganha.
  Se um dos jogadores desistir do jogo, o outro ganha. 

-- R E G R A S --

  -O jogo não permite que o jogador ultrapasse os 100 pontos do card; 
  -Uma vez definida o valor do atributo, este não poderá ser mudado após confirmar (apertar o ENTER).

