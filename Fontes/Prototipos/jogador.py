import random #https://docs.python.org/2/library/random.html
import socket
import string
import os
import sys
import time 

def limparTela():
	os.system("cls")
	#os.system("clear")
	#print("Limpar tela")

def menu(nome):
	while(True):
		limparTela()
		print "Ola "+nome+",\n\
	vamos jogar?\n"
		menu = '''
	===============MENU==============
	| Digite o numero referente	|
	|    a acao desejada		|
	|				|
	| 1 - Jogar			|
	| 2 - Ver regras		|
	| 3 - Sair			|
	=================================

	'''
		print menu
		opcao = input()
		if opcao == 1:
			socket = conectarServidor()
			carta = gerarCarta()
			print "GEROU CARTA"
			print "TESTE"
			print type(socket)
			print type(carta)
			print carta
			socket.send(carta)

			mensagemDoServidor = receberResposta(socket)
			exibirResposta(mensagemDoServidor)

			raw_input("acabou a opcao")
		elif opcao == 2:
			verRegras()
		elif opcao == 3:
			sair()
		else:
			print "Ops! Essa opcao nao existe. "

	#print menu
def sair():
	limparTela()
	print "Tchaaau! :)\n"
	sys.exit(0)

def verRegras():
	limparTela()
	print '''
	======================REGRAS=======================
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	|REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS REGRAS |
	===================================================
	'''
	raw_input("Aperte ENTER para voltar ao Menu.")

def conectarServidor():
	servidor, porta = enderecoDefault()
	socket = conectar(servidor, porta)
	aguardarJogador()
	return socket

def aguardarJogador():
	print "Aguardando jogador... PARA VER ISSO AQUI"
	time.sleep(1)
	# print "3"
	# time.sleep(1)
	# print "2"
	# time.sleep(1)
	# print "1"
	# time.sleep(1)
	print "Jogador encontrado."


def gerarCarta():
	carta = "0;0;0;0;0"
	atributo1,atributo2,atributo3,atributo4,atributo5 = 0,0,0,0,0
	MAX = 100

	limparTela()
	print ''' Esta na hora de criar sua carta
Voce tem 100 pontos para distribuir em 5 atributos.
	Veja sua carta...
	'''
	#atributo ATAQUE
	mostrarCarta(atributo1,atributo2,atributo3,atributo4,atributo5,MAX)
	atributo1 = input("Quantos pontos quer adicionar em Ataque?\n")
	while atributo1 > MAX:
		atributo1 = input("Voce nao tem tantos pontos.\n\
  Quantos pontos gostaria de adicionar em Ataque?\n")
	MAX = MAX - atributo1

	#atributo DEFESA
	limparTela()
	print "Prontinho. Veja sua carta..."
	mostrarCarta(atributo1,atributo2,atributo3,atributo4,atributo5,MAX)
	atributo2 = input("Quantos pontos quer adicionar em Defesa?\n")
	while atributo2 > MAX:
		atributo2 = input("Voce nao tem tantos pontos.\n\
  Quantos pontos gostaria de adicionar em Defesa?\n")
	MAX = MAX - atributo2
	
	#atributo INTELIGENCIA
	limparTela()
	print "Otimo. Veja sua carta..."
	mostrarCarta(atributo1,atributo2,atributo3,atributo4,atributo5,MAX)
	atributo3 = input("Quantos pontos quer adicionar em Inteligencia?\n")
	while atributo3 > MAX:
		atributo3 = input("Voce nao tem tantos pontos.\n\
  Quantos pontos gostaria de adicionar em Inteligencia?\n")
	MAX = MAX - atributo3
	
	#atributo FORCA
	limparTela()
	print "Veja sua carta..."
	mostrarCarta(atributo1,atributo2,atributo3,atributo4,atributo5,MAX)
	atributo4 = input("Quantos pontos quer adicionar em Forca?\n")
	while atributo4 > MAX:
		atributo4 = input("Voce nao tem tantos pontos.\n\
  Quantos pontos gostaria de adicionar em Forca?\n")
	MAX = MAX - atributo4
	
	#atributo AGILIDADE
	atributo5 = MAX
	MAX = MAX - atributo5
	limparTela()
	print "Tudo certo, adicionamos o restante em Agilidade."
	print "  Veja como esta sua carta"
	mostrarCarta(atributo1,atributo2,atributo3,atributo4,atributo5,MAX)


	carta = str(atributo1)+";"+str(atributo2)+";"+str(atributo3)+";"+str(atributo4)+";"+str(atributo5)
	print "TESTE: "+carta

	raw_input("PAUSE... Aperte ENTER para voltar ao Menu.")
	carta = "1;2;3"
	return carta;

	
def mostrarCarta(a,b,c,d,e,x):
	carta = "\nPontos para distribuir: "+str(x)
	carta += "\n\n[=======CARD=======]"
	carta += "\n[ Ataque	"+str(a)+" ]"
	carta += "\n[ Defesa	"+str(b)+" ]"
	carta += "\n[ Inteligencia	"+str(c)+" ]"
	carta += "\n[ Forca		"+str(d)+" ]"
	carta += "\n[ Agilidade	"+str(e)+" ]"
	carta += "\n[==================]\n"
	print carta
	return carta

def enderecoDefault():
	servidor = 'localhost'
	porta = 12345
	return servidor, porta

def gerarNome():
	nome = ""
	nome += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	nome += str(random.randint(1,9))
	nome += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	nome += str(random.randint(1,9))
	print "Ola, aqui quem fala e o "+nome

	return nome

def imprimirCarta(a1, b1, c1, a2, b2, c2):
	a1, b1, c1, a2, b2, c2 = tratarStringServidor(resposta)
	carta = ""

	return carta

def tratarStringServidor(resposta):
	aux = resposta.split(";")
	aux[0], aux[1], aux[2], aux[3], aux[4], aux[5] =  a1, b1, c1, a2, b2, c2
	return a1, b1, c1, a2, b2, c2

def enviarMensagem(socket, mensagem):
	socket.send(mensagem)
	return mensagem

def conectar(servidor, porta):

	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	clientSocket.connect((servidor, porta))
	print "Conectado ao servidor "+servidor+" na porta "+str(porta)+"."
	return clientSocket

def fechar(clientSocket):
	clientSocket.close()

def mensagemRecebida():
	return "Mensagem recebida do servidor"

def pegarNome():
	nome = raw_input("Qual seu nome?\n")
	return nome


def play():
	limparTela()
	nome = "Testador" #pegarNome()
	menu(nome)

def receberResposta(socket):
	resposta = socket.recv(8126)
	print resposta
	return resposta

def exibirResposta(msg):
	print msg
	raw_input("PAUSE... Aperte ENTER para voltar ao Menu.")
	return msg

play()