import random #https://docs.python.org/2/library/random.html
import socket
import string
import os
import sys

MAX = 20

def enderecoDefault():
	servidor = 'localhost'
	porta = 12345
	return servidor, porta

def limparTela():
	limpar = "gambiarra"
	#os.system("cls")
	#os.system("clear")
	#print("Limpar tela")

def sair():
	print "Tchaaau! :)\n"
	sys.exit(0)

def gerarCarta():
	carta = "0;0;0"
	global MAX
	aux = MAX
	atributo1 = random.randint(0,aux)
	aux = aux - atributo1
	atributo2 = random.randint(0,aux)
	aux = aux - atributo2
	atributo3 = aux
	carta = str(atributo1)+";"+str(atributo2)+";"+str(atributo3)
	return carta;

def gerarNome():
	nome = ""
	nome += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	nome += str(random.randint(1,9))
	nome += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	nome += str(random.randint(1,9))
	print "Ola, aqui quem fala e o "+nome+"\n\
	Vamos jogar!"
	return nome

def imprimirCarta(a1, b1, c1, a2, b2, c2):
	a1, b1, c1, a2, b2, c2 = tratarStringServidor(resposta)
	carta = "imprimirCarta"

	return carta

def tratarStringServidor(resposta):
	aux = resposta.split(";")
	aux[0], aux[1], aux[2], aux[3], aux[4], aux[5] =  a1, b1, c1, a2, b2, c2
	return a1, b1, c1, a2, b2, c2

def enviarMensagem(socket, mensagem):
	socket.send(mensagem+"\n")
	return mensagem

def conectar(servidor, porta):

	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	clientSocket.connect((servidor, porta))
	return clientSocket

def fechar(clientSocket):
	clientSocket.close()

def mensagemRecebida():
	return "Mensagem recebida do servidor"

def play():

	limparTela()
	nome = gerarNome()
	carta = gerarCarta()

	servidor,porta = enderecoDefault()
	print "\n\nEncontrou servidor:porta: "+servidor+":"+str(porta)

	s = conectar(servidor,porta)
	print "Conectou no socket: "+str(s)
	#conectar(enderecoDefault()[0], enderecoDefault()[1])
	#enviarMensagem(s,nome) #enviar nome para o servidor
	#print "Mensagem enviada: "+enviarMensagem(s,nome)
	enviarMensagem(s,carta) #enviar carta para o servidor
	print "Mensagem enviada: "+enviarMensagem(s,carta)
	msg = mensagemRecebida()
	print msg

	fechar(s) #fechar socket
	print "Fechado a conexao: "+str(s)

play()

