// package br.ufg.inf.es.rcsd.lab1;

import java.io.*;
import java.net.*;
import java.util.Scanner;

class PrototipoCliente {
    public static boolean sair;
    public static String servidor = "localhost";
    public static int porta = 6789;
    
    //gambiarras
    public static String nomePlayer2 = "Beltrano";
    public static String jogador = "Fulano";
    
	public static void main(String argv[]) throws Exception {
            System.out.println("jogo comecando");
            apresentarMenu();
            System.out.println("jogo acabando...");
	}

	//nao mexi nada nesse metodo. so tirei as coisas que peguei do template do akira e coloquei aqui dentro... por favor verificar tudo.
	public static void metodoDoAkira(String ip, int porta) throws IOException{ 
		String sentence;
		String modifiedSentence;

		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Conectando ao servidor " + servidor + ":" + porta);

		Socket clientSocket = new Socket(servidor, porta);

		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		System.out.println("Digite string a ser enviada para o servidor");
		sentence = inFromUser.readLine();

		outToServer.writeBytes(sentence + '\n');

		modifiedSentence = inFromServer.readLine();

		System.out.println("Recebido do servidor: " + modifiedSentence);

		clientSocket.close();
	}

	public static void apresentarMenu() throws IOException{
		System.out.println("[==========================MENU==========================]");
		System.out.println("[= Digite 1 caso queira ver as regras do jogo ===========]");
		System.out.println("[= Digite 2 caso queira procurar um player para jogar ===]");
		System.out.println("[= Digite 3 caso queira alterar seu nome ================]");
		System.out.println("[= Digite 4 para sair ===================================]");
		System.out.println("[========================================================]\n");

		Scanner escolha = new Scanner(System.in);
		while(true) {
			switch(escolha.nextInt()){
				case 1:
					apresentarRegras();
					break;
				case 2:
					conectarAoServidor();
					break;
				case 3:
					alterarNome();
					break;
				case 4:
					sair();
					break;
				default: 
					System.out.println("Ops, nao existe essa opcao... :(");
			}
		}
	}

	public static void apresentarRegras() throws IOException{
		System.out.println("[=========================REGRAS==========================]");
		System.out.println("[= * JOKENPO CARDS é um jogo multiplayer que voce cria ===]");
		System.out.println("[=== cartas com pontos distribuidos em 3 atributos: Atk, =]");
		System.out.println("[=== Def e Int. O jogo compara sua carta com a carta do  =]");
		System.out.println("[=== seu oponente e quem ganhar os pontos em 2 atributos =]");
		System.out.println("[=== ganha a rodada. A partida é uma \"melhor de tres\", ===]");
		System.out.println("[=== seja, quem ganhar 2 rodadas ganha o jogo. ===========]");
		System.out.println("[=========================================================]");
		System.out.println("[= * Digite ENTER para voltar ao menu ====================]");
		System.out.println("[=========================================================]\n");
		System.in.read();
                apresentarMenu();
	}

	public static void alterarNome() throws IOException {
		System.out.println("Ola jogador, qual e o seu nome?");
		Scanner nome = new Scanner(System.in);
		nome.next();
		System.out.println("Ola " +nome);
                apresentarMenu();
	}

    private static void conectarAoServidor() throws IOException {
        System.out.println("Conectando...");
        System.in.read();
        System.out.println("Aguardando jogadores...");
        System.in.read();
        System.out.println("Pronto! Voce ira jogar com "+nomePlayer2+".");
        System.in.read();
        System.out.println("Faça sua carta:");
        System.in.read();
        fazerCarta();
    }

    private static void sair() {
        System.out.println("SAIUUUUU!!!!");
        sair = true;
    }

    private static void fazerCarta() throws IOException {
        System.out.println(jogador+", crie sua carta.");
        System.out.println("Voce tem 10 pontos para distribuir em 3 atributos.");
        leiaValoresCarta();
        //gambiarra pra funcionar
        int p=10, a=0, d=0, i=0;
        mostrarCarta(p,a,d,i);
        
        System.out.println("quantos pontos quer colocar em Atk?");
        System.in.read();
        p=5; a=5; d=0; i=0;
        mostrarCarta(p,a,d,i);
        System.out.println("Quantos pontos quer colocar em Def?"
                + "\nVale lembrar que o valor restante e adicionado em Int.");
        System.in.read();
        p=0; a=5; d=1; i=4;
        mostrarCarta(p,a,d,i);
        System.out.println("A sua carta ficara assim:");
        mostrarCartaPronta(a, d, i);
        System.out.println("Pode enviar para a mesa?");
        System.in.read();
        enviarCarta();
    }

    private static void mostrarCarta(int p, int a, int d, int i) {
        String carta = ""
                + "Pontos diponiveis: "+p+"\n"
                + "[===Carta1===]\n"
                + "[=Atk......."+a+"]\n"
                + "[=Def......."+d+"]\n"
                + "[=Int......."+i+"]\n"
                + "[============]";
        System.out.println(carta);
    }

    private static void leiaValoresCarta() {
        int p=10, a=0, d=0, i=0;
        //return p, a, d, i; //fazer essa porra retornar...
    }

    private static void enviarCarta() throws IOException {
        System.out.println("Carta enviada. Aguarde enquanto o outro jogador envie a dele...");
        System.in.read();
        apresentarResultadoRodada();
    }

    private static void apresentarResultadoRodada() {
        System.out.println("Parabens Fulaninho! Voce ganhou a rodada.\n"
                + "Veja as cartas");
        int a1 = 5,
            d1 = 1,
            i1 = 4,
            a2 = 3,
            d2 = 5,
            i2 = 2;
        compararCartas(a1,d1,i1,a2,d2,i2);
        
    }

    private static void mostrarCartaPronta(int a, int d, int i) {
        String carta = ""
                + "[===Carta1===]\n"
                + "[=Atk......."+a+"]\n"
                + "[=Def......."+d+"]\n"
                + "[=Int......."+i+"]\n"
                + "[============]";
        System.out.println(carta);
    }

    private static void compararCartas(int a1, int d1, int i1, int a2, int d2, int i2) {
                String carta = ""
                + "\nCarta1 carta de " + jogador
                + "\nCarta2 carta de " + nomePlayer2
                + "\n[===Carta1===]\t[===Carta2===]"
                + "\n[=Atk......."+a1+"]\t[=Atk......."+a2+"]"
                + "\n[=Def......."+d1+"]\t[=Def......."+d2+"]"
                + "\n[=Int......."+i1+"]\t[=Int......."+i2+"]"
                + "\n[============]\t[============]";
        System.out.println(carta);
    }
}