
import java.util.ArrayList;
import java.util.Random;

public class Operacoes {
    
    public static ArrayList<Carta> cartas;
    public static int check = 0;
    
    public static int adicionarCarta(Carta carta) {        
        limparCartas();
        
        if(cartas == null) {
            cartas = new ArrayList<>();
        }
        
        cartas.add(carta);
        
        return cartas.indexOf(carta);
    }
    
    public static void limparCartas() {
        if(check == 2) {
            cartas.clear();
            check = 0;
        }
    }
    
    public static Carta gerarCarta(){
   	 
        int max = 20;

        Carta carta = new Carta();
        Random gerador = new Random();

        carta.setAgilidade(gerador.nextInt(max));
        max = max - carta.getAgilidade();
        carta.setForca(gerador.nextInt(max));
        max = max-carta.getForca();
        carta.setInteligencia(max);

        return carta;
    }

    public static Carta formarCarta(String recebido){
        Carta carta = new Carta();

        String aux[] = recebido.split(";");
        carta.setAgilidade(Integer.parseInt(aux[0]));
        carta.setForca(Integer.parseInt(aux[1]));
        carta.setInteligencia(Integer.parseInt(aux[2]));
        
        return carta;
    }

    public static int compararCartas() throws Exception{        
        boolean esperando = true;
        int ganhador = 0;
        
        while (esperando) {
            
            if(cartas.size() == 2) {
                System.out.println("Calculandno");
                Carta carta1 = cartas.get(0), carta2 = cartas.get(1);
                
                int pontosPlayer1 = 0, pontosPlayer2 = 0;
                
                if(carta1.getAgilidade() > carta2.getAgilidade()) {
                pontosPlayer1++;
                //System.out.println("Jogador 1 ganhou\n");
                } else {
                if(carta1.getAgilidade() < carta2.getAgilidade()) {
                pontosPlayer2++;
                //System.out.println("Jogador 2 ganhou\n");
                }
                }

                if(carta1.getForca() > carta2.getForca()) {
                pontosPlayer1++;
                //System.out.println("Jogador 1 ganhou\n");
                } else {
                if(carta1.getForca() < carta2.getForca()) {
                pontosPlayer2++;
                //System.out.println("Jogador 2 ganhou\n");
                }
                }

                if(carta1.getInteligencia() > carta2.getInteligencia()) {
                pontosPlayer1++;
                //System.out.println("Jogador 1 ganhou\n");
                } else {
                if(carta1.getInteligencia() < carta2.getInteligencia()) {
                pontosPlayer2++;
                //System.out.println("Jogador 2 ganhou\n");
                }
                }  	 
                
                if(pontosPlayer1 > pontosPlayer2) {
                ganhador = 1;
                } else {
                if(pontosPlayer1 < pontosPlayer2) {
                ganhador = 2;
                } else {
                ganhador = 0;
                }
                }
                esperando = false;                
            } else {
                Thread.sleep(100);
            }
        }
        return ganhador;
    }
}
