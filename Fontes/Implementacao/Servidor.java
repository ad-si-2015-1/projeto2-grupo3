import java.net.*;
import java.util.ArrayList;

public class Servidor extends Thread{
    public static void main (String[] args) throws Exception{
        int porta = 12345;
        
        ServerSocket server = new ServerSocket(porta);
        while (true) {
            Socket conexao = server.accept();
            ConexaoJogador jogador = new ConexaoJogador(conexao);
            adicionar(jogador);
            
            Thread jogo = new Thread(jogador);
            jogo.start();
        }        
    }

    
    static ArrayList<ConexaoJogador> conexoes = new ArrayList<>();
        
    public static void adicionar(ConexaoJogador jogador) {
    conexoes.add(jogador);
    System.out.println("Conexao com jogador feita");        
    }

    
}
