
public class Carta {
    private int agilidade;
    private int forca;
    private int inteligencia;

    public Carta() {
    }

    public Carta(int agilidade, int forca, int inteligencia) {
        this.agilidade = agilidade;
        this.forca = forca;
        this.inteligencia = inteligencia;
    }

    public int getAgilidade() {
        return agilidade;
    }

    public int getForca() {
        return forca;
    }

    public int getInteligencia() {
        return inteligencia;
    }

    public void setAgilidade(int agilidade) {
        this.agilidade = agilidade;
    }

    public void setForca(int forca) {
        this.forca = forca;
    }

    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }
    
    public String virarString() {
        return agilidade + ";" + forca + ";" + inteligencia;
    }

    public void show(){

    String  show = "Agilidade: " + agilidade + " "
            + "Forca: " + forca + " "
            + "Inteligencia: " + inteligencia;
        System.out.println(show);
    }
}

