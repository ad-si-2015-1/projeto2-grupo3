import java.net.*;
import java.io.*;

public class ConexaoJogador implements Runnable{
    Socket conexao;
    BufferedReader entrada;
    DataOutputStream saida;
    String nome;
    Carta carta;
    
    public ConexaoJogador(Socket conexao) throws Exception{
        this.conexao = conexao;
        entrada = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
        saida = new DataOutputStream(conexao.getOutputStream());
    }
    
    public void enviarMensagem(String mensagem) throws Exception {
        saida.writeBytes(mensagem + "\n");        
    }
    
    public String receberMensagem() throws Exception {
        return entrada.readLine();        
    }
    
    public void login() throws Exception{
        saida.writeBytes("Digite seu nome: \n");
        nome = entrada.readLine();
        System.out.println(nome);
    }
    
    public void serCarta(Carta carta) {
        this.carta = carta;
    }    
    
    @Override
    public void run() {
        try {
            
            enviarMensagem("Envie sua carta\n");            
            carta = Operacoes.formarCarta(receberMensagem());
            
            //Adicina sua carta para comparacao e um indicador de player 1 ou 2
            int player = Operacoes.adicionarCarta(carta) + 1;
            
            //Faz a comparação das cartas
            int ganhador = Operacoes.compararCartas();
            
            String envioFinal;
            
            if(player == 1) {
                envioFinal = carta.virarString() + ";" + Operacoes.cartas.get(1).virarString() + ";";
            } else {
                envioFinal = carta.virarString() + ";" + Operacoes.cartas.get(0).virarString() + ";";
            }
            
            if(ganhador == player) {
                envioFinal += "Vitoria!";
            } else if(ganhador == 0) {
                envioFinal += "Empate!";
            } else {
                envioFinal += "Derrota!";
            }
            enviarMensagem(envioFinal);
            conexao.close();
            Operacoes.check++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}